package com.example.warCard;

import com.example.warCard.deck.Deck;
import com.example.warCard.deck.PlayingCard;

import java.util.ArrayDeque;
import java.util.Queue;

class GamePlay {
    private static final String ANSI_RESET = "\u001B[0m";
    private static final String ANSI_RED = "\u001B[31m";
    private static final int MAX_ROUND = 10_000;

    private final Player player1;
    private final Player player2;
    private final Deck deck;

    private int round;
    private Queue<PlayingCard> cardsOnTheTable = new ArrayDeque<>();

    GamePlay(Player player1, Player player2, Deck deck) {
        this.player1 = player1;
        this.player2 = player2;
        this.deck = deck;
    }

    void gameStart() {
        prepareGame();
        round = 1;
        while (player1.getQuantityOfCards() != 0 && player2.getQuantityOfCards() != 0 && round <= MAX_ROUND) {
            printHeader("Round: " + round);
            particularRound();
            round++;
        }
        if (round > MAX_ROUND) {
            printHeader("DRAW - no winner. You played " + MAX_ROUND + " rounds. " +
                    "\nAssuming each round takes 4 seconds, you played more than 11 hours." +
                    "\nDon't waste time, leave cards and code more in Java!!!!");
        } else if (player1.getQuantityOfCards() == 0 && player2.getQuantityOfCards() != 0) {
            printHeader("End of game. " + player2.getNick() + " is a WINNER!!!");
        } else if (player2.getQuantityOfCards() == 0 && player1.getQuantityOfCards() != 0) {
            printHeader("End of game. " + player1.getNick() + " is a WINNER!!!");
        }
    }

    private void prepareGame() {
        printHeader("New Deck before unboxing - peek on a left");
        System.out.println(deck);
        deck.shuffle();
        printHeader("Deck after shuffle  - peek on a left");
        System.out.println(deck);
        dealDeckBetweenTwoPlayers(deck);
        printHeader("Deck dealt between player1 and player2-  - peeks on a left");
        printPlayersCards();
    }

    private void particularRound() {
        //case when there is WAR
        if (player1.getPeek().compareTo(player2.getPeek()) == 0) {
            warCase();
            //no war, player1 win
        } else if (player1.getPeek().compareTo(player2.getPeek()) > 0) {
            noWarCase(player1);
        } //no war, player2 win
        else {
            noWarCase(player2);
        }
    }

    private void warCase() {
        //case when both players have the same deck of 26 cards in hand sorted in the same order
        if (player1.getQuantityOfCards() <= 2 && player2.getQuantityOfCards() <= 2) {
            drawDuringWar();
            //case when player2 has just 2 or 1 card in hand and there will be war, player2 loses automatically
        } else if (player2.getQuantityOfCards() <= 2) {
            endOfGame(player1, player2);
            //case when player1 has just 2 or 1 card in hand and there will be war, player1 loses automatically)
        } else if (player1.getQuantityOfCards() <= 2) {
            endOfGame(player2, player1);
            // standard war
        } else {
            //first stage - cards face-up they are equal
            playersPutPeekCardOnTable(" ");
            printInRed("WAR !!!!!!!!!!!!!!!");
            //second stage - cards face-down
            playersPutPeekCardOnTable(" (face down) ");
            particularRound();
        }
    }

    private void printPlayersCards() {
        System.out.println(player1);
        System.out.println(player2);
    }

    private void noWarCase(Player winner) {
        playersPutPeekCardOnTable(" ");
        winner.addQueueOfCards(cardsOnTheTable);
        System.out.println("Cards on the table: " + cardsOnTheTable);
        System.out.println("Round " + round + " winner is: " + winner.getNick());
        printPlayersCards();
        cardsOnTheTable.clear();
    }

    private void drawDuringWar() {
        playersPutPeekCardOnTable(" ");
        printInRed("WAR !!!!!!!!!!!!!!!");
        playersPutPeekCardOnTable(" (face down) ");
        System.out.println("Cards on the table: " + cardsOnTheTable);
        printPlayersCards();
        printHeader("DRAW - no winner. Players do not have more cards to finish WAR");
    }

    /**
     * case when one player has one or two cards in hand and this is time for a war
     * player does not have enough cards to finish war, so the game is over
     */
    private void endOfGame(Player winner, Player looser) {
        for (int i = 0; i < looser.getQuantityOfCards(); i++) {
            playersPutPeekCardOnTable(" ");
            if (i == 0) {
                printInRed("WAR !!!!!!!!!!!!!!!");
            }
        }
        winner.addQueueOfCards(cardsOnTheTable);
        System.out.println("Cards on the table: " + cardsOnTheTable);
        printPlayersCards();
        cardsOnTheTable.clear();
        printHeader(looser.getNick() + " does not have more cards, and cannot finish WAR.");
    }

    private void printHeader(String text) {
        System.out.println("\n--------------------------------------------------------------------------");
        System.out.println(" *** " + text + " ***");
        System.out.println("--------------------------------------------------------------------------");
    }

    private void printInRed(String text) {
        System.out.println(ANSI_RED + text + ANSI_RESET);
    }

    private void playersPutPeekCardOnTable(String text) {
        System.out.println(player1.getNick() + ": " + player1.getPeek() + text);
        System.out.println(player2.getNick() + ": " + player2.getPeek() + text);
        cardsOnTheTable.add(player1.getPeek());
        cardsOnTheTable.add(player2.getPeek());
        player1.removeCard();
        player2.removeCard();
    }

    private void dealDeckBetweenTwoPlayers(Deck deck) {
        for (int i = 0; i < deck.getSize(); i++) {
            if (i % 2 != 0) {
                player1.addCard(deck.getElement(deck.getSize() - 1 - i));
            } else {
                player2.addCard(deck.getElement(deck.getSize() - 1 - i));
            }
        }
    }
}

