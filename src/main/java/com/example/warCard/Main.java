package com.example.warCard;

import com.example.warCard.deck.Deck;

public class Main {
    public static void main(String[] args) {
        GamePlay gamePlay = new GamePlay(new Player("kiler"), new Player("ryba"), new Deck());
        gamePlay.gameStart();
    }
}
