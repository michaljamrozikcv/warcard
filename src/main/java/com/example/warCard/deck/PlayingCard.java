package com.example.warCard.deck;

public class PlayingCard implements Comparable<PlayingCard> {
    private final Rank rank;
    private final Suit suit;

    PlayingCard(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    @Override
    public String toString() {
        return rank.getRank() + suit.getSymbol();
    }

    @Override
    public int compareTo(PlayingCard playingCard) {
        return this.rank.compareTo(playingCard.rank);
    }
}
