package com.example.warCard.deck;

import com.example.warCard.Player;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Deck {
    private final List<PlayingCard> listOfCards = Stream.of(Rank.values())
            .flatMap(rank -> Stream.of(Suit.values())
                    .map(suit -> new PlayingCard(rank, suit)))
            .collect(Collectors.toList());

    public int getSize() {
        return listOfCards.size();
    }

    public PlayingCard getElement(int index) {
        return listOfCards.get(index);
    }

    public void shuffle() {
        Collections.shuffle(listOfCards);
    }

    @Override
    public String toString() {
        return listOfCards.toString();
    }
}


