package com.example.warCard.deck;

enum Suit {
    CLUB('\u2663'),
    DIAMOND('\u2666'),
    HEART('\u2665'),
    SPADE('\u2660');

    private final char symbol;

    Suit(char symbol) {
        this.symbol = symbol;
    }

    public char getSymbol() {
        return symbol;
    }
}
