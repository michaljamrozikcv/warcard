package com.example.warCard;

import com.example.warCard.deck.PlayingCard;

import java.util.ArrayDeque;
import java.util.Queue;

public class Player {
    private final String nick;
    private int quantityOfCards;
    private final Queue<PlayingCard> playingCards = new ArrayDeque<>();

    public Player(String nick) {
        this.nick = nick;
    }

    String getNick() {
        return nick;
    }

    int getQuantityOfCards() {
        return quantityOfCards;
    }

    private Queue<PlayingCard> getPlayingCards() {
        return playingCards;
    }

    PlayingCard getPeek() {
        return playingCards.peek();
    }

    void addCard(PlayingCard playingCard) {
        playingCards.add(playingCard);
        quantityOfCards++;
    }

    void addQueueOfCards(Queue<PlayingCard> cards) {
        playingCards.addAll(cards);
        quantityOfCards += cards.size();
    }

    void removeCard() {
        playingCards.remove();
        quantityOfCards--;
    }

    @Override
    public String toString() {
        return getNick() + ": " + getPlayingCards() + " quantity of cards: " + getQuantityOfCards();
    }
}
