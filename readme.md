This is a game for 2 players and 1 deck of 52 standard cards.

This is single game, that means you unbox deck of cards, shuffle and play just one game.

Rules are easy - deck is being shuffled, then the deck is divided evenly among the players.

You can see how many rounds are needed to finish a game, and print all rounds.

At very beginning each player has 26 cards, cards behave like queue.

1st player takes one card (peek of his queue), put on a table.

2nd player takes one card (peek of his queue), put on 1st player's card.

Higher ranked card win (suit doesn't matter), then player who won, takes cards, and add them into his queue at 
the end.(card which is added first is  player1's card, then player2's card).

If cards are equal (have the same rank), it is time for a WAR!

Players create "cards in war status" queue:
'#1 and #2 cards - already laying on a table.
'#3 card: 1st player takes next card (peek of his queue), put on #2 card face down.
'#4 card: 2nd player takes next card (peek of his queue), put on #3 card face down.
'#3 and #4 cards do not fight.
'#5 card: 1st player takes next card (peek of his queue), put on #4 card.
'#6 card: 2nd player takes next card (peek of his queue), put on #5 card.

We compare #5 to #6: if player1 win, he takes whole "cards in war status" list from a table and add to his queue at the bottom.
(card #1 is added firstly, then #2, then #3 etc. If player2 win, he takes whole "cards in war status"
queue from a table and add to his queue at the bottom.

It is possible, we have WAR again, so process needs to be repeated.

Player who collect whole deck of cards is a WINNER!

In case if one of players have 1 or 2 cards, and this is a WAR time, player automatically looses.

In case when both players have half of deck (so 26 cards) in hands sorted in the same order 
(so in fact they have the same cards if we not take suit into consideration -  it is DRAW.

If there is still no winner, game is finished after playing 10,000 rounds, and this is DRAW no matter how many cards
players have.